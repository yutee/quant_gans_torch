# Quant_GANs_Torch

## Name
Implementation of "Quant GANs: Deep Generation of Financial Time Series" (https://arxiv.org/abs/1907.06673) Using PyTorch.

## Description
<li>
<ol>This project seeks to implement the GAN described in the abovementioned paper. Using two different NNs as opponents is the fundamental principle of GANs. While one NN, the so-called generator, is responsible for the generation of stock price paths, the second one, the discriminator, has to judge whether the generated paths are synthetic or from the same underlying distribution as the data (i.e. the past prices).</ol>
<ol>The notebook quant_gans_torch.ipynb seeks to replicate the numerical study (section 7 of paper) applying Quant GANs to the S&P 500 index from May 2009 - December 2018.</ol>
<ol>Data for the study is obtained from Yahoo Finance API, and by adjusting the parameters for the API call, data for different stock symbols, start date and/or end date can be used as well.</ol> 
<ol>Code for training the GAN is presented in the notebook.</ol>
<ol>Visualization correponding to those presented in pages 24 and 25 of the paper are also generated.</ol>
</li>

## Installation
<li>
<ol>The Jupyter notebook(s) presented in this repository is developed and tested in Google Colab Pro.</ol>
<ol>requirements.txt concerned is generated by session_info (https://pypi.org/project/session-info/).</ol>
</li>

## Usage
<li>
<ol>Run all cells of the Jupyter notebook to obtain the baseline result of the study.</ol>
<ol>If desired, the parameters for the API call retrieving data for Yahoo Finance API can be adjusted in the "Step 3" section of the notebook. </ol>
</li>

## Acknowledgment
Reference is made to:
<li>
<ol>https://github.com/ICascha/QuantGANs-replication (for solution reference developed in Tensorflow)</ol>
<ol>https://github.com/locuslab/TCN (for TCN implementation)</ol>
</li>

